import React, { Component } from "react";
import './post.css'
export default class Post extends Component{ 
    render(){
        const nickname = this.props.nickname;
        const avatar = this.props.avatar;
        const image = this.props.image;
        const caption = this.props.caption;
        const likes=this.props.likes;
        return(
            <article className="Post" ref="Post">
            <header>
              <div className="Post-user">
                <div className="Post-user-avatar">
                  <img src={avatar} alt={nickname} />
                </div>
                <div className="Post-user-nickname">
                  <span>{nickname}</span>
                </div>
              </div>
            </header>
            <div className="Post-image">
              <div className="Post-image-bg">
                <img alt={caption} src={image} />
              </div>
            </div>
            <div className="Post-caption">
              <strong>{nickname}</strong> {caption}
            </div>
            <div className="likes">{likes}</div>
          </article>)
    }
}