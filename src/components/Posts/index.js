import React, { Component } from "react";
import Post from "../Post/index";
import posts from "../../data"
export default class Posts extends Component{ 
    constructor(){
        super();
        this.state = posts;
      }
    render() {
        return (
            <div className="Posts">
              {this.state
                .slice(0)
                .reverse()
                .map(post => (
                  <Post
                    nickname={post.nickname}
                    avatar={post.avatar}
                    image={post.image}
                    caption={post.caption}
                    likes={post.likes}
                  />
                ))}
            </div>
          
        )
    }
  };
