import React from 'react';
import './App.css';
import Posts from './components/Posts/index'
import NewPost from './components/New post/index'


function App() {
  return (
    <div className="App">
      <NewPost />
        <Posts />
    </div>
  );
}

export default App;
